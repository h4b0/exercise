require "./1.rb"
describe "exer1" do
  it "should return 23 for 9" do
    expect(exer1 9).to eq(23)
  end

  it "should return 5 for 10" do
    expect(exer1 10).to eq(33)
  end

  it "should return 5 for 10" do
    expect(exer1 1000).to eq(234168)
  end

end
