def mix_word word
  word.sub(/(?<=\w)\w{2,}(?=\w)/){ |mix| mix.chars.shuffle.join}
end

def swap_text(string)
  string.gsub(/\w{3,}+/){|str| mix_word str}
end

#puts swap_text "Hi I'am, Bilbo Czesc Nowy Model Zadania"

describe "swap_text" do
  it "return mixed text" do
    expect(swap_text("John").chars).to match_array("John".chars)
  end

  it "return mixed text" do
    expect(swap_text("Hi, I'am John Snow \n From Winterfell").chars).to match_array("Hi, I'am John Snow \n From Winterfell".chars)
  end

  it "has that same latter on beginin" do
    expect(swap_text("John")[0]).to eq("J")
  end

  it "has that same latter on end" do
    expect(swap_text("John Snow")[-1]).to eq("w")
  end

end

