class Human
  attr_accessor :name
  def initialize(current_name)
    @name = current_name
  end
end

class Student < Human
  def method_missing(method_name)
    if method_name.to_s.end_with?('?')
        method_name.to_s == @name+"?"
    else
      super
    end
  end

  def respond_to_missing?(method_name, include_private = false)
    method_name.to_s.end_with?('?') || super
  end
end

describe "method_missing" do
  before :each do
      @b=Student.new("zosia")
  end
  context "valid data" do
    it "return true" do
      expect(@b.zosia?).to eq(true)
    end
  end

  context "invalid data" do
    it "return false" do
      expect(@b.krysia?).to eq(false)
    end
  end

  context "invalid method" do
    it "return error" do
      expect{@b.zosia}.to raise_error
    end
  end
  context "respond :zosia" do
    it "return true" do
      expect(@b.respond_to?(:krysia))
    end
  end
end


