class Student
  include Comparable
  attr_reader :avg_marks, :group
  def initialize(marks)
    @avg_marks =  ((marks.reduce(:+).to_f)/marks.size)
  end

  def <=> (other_student)
    @avg_marks <=> other_student.avg_marks
  end
end


class StudentsGroup
  include Enumerable

  def initialize(*members)
    @members = members
    @members.sort!{|x,y| y.avg_marks<=>x.avg_marks}
  end

  def each &block
    @members.each do |member|
      if block_given?
        block.call member
      else
        yield member
      end
    end
  end
  def[](number)
    @members[number]
  end

  def avg_marks
    @members.reduce(0.0){|v,student|v+student.avg_marks}/@members.size
  end
end




describe Student do
  describe "comparable" do
    before :each do
      @student1 = Student.new([4.0,5])
      @student2 = Student.new([3.0,2])
      @student3 = Student.new([4,5.0])
    end

    context "check if first student have the same avg like third" do
      it "return true" do
        @student1 == @student3
      end
    end

    context "check if first student have better avg_marks than second" do
      it "return true" do
        @student1 > @student2
      end
    end
  end
end

describe StudentsGroup do
  before :each do
      @student1 = Student.new([5,5])
      @student2 = Student.new([5,4.0])
      @student3 = Student.new([5,4.5])
      @group = StudentsGroup.new(@student1,@student2,@student3)
  end

  context "ordering desc by avg_marks" do
    it "return student with max avg" do
      expect(@group[0]).to eq(@student1)
    end
  end

  context "avg_marks for group" do
    it "return properly value" do
      expect(@group.avg_marks).to eq(4.75)
    end
  end

end

