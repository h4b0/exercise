#!/usr/bin/ruby
def add_to_sieve primes
  1000.times { primes << false }
end

def sieve primes
  primes_counter=1
  (0..primes.count).each.with_index(2) do |item,index|
    break if index*index >= primes.count
    next if primes[index]
    (index*2..primes.count).step(index).each do |index2|
      primes[index2] = true
      print "index1:#{index} index2:#{index2}\n"
    end
  end
  (0..primes.count).each.with_index(2) do |item,index|
    primes_counter += 1 unless primes[index]
  end
  primes_counter
end

def nth_prime n
  primes = Array.new(1000,false)

  while sieve(primes) <= n
    add_to_sieve primes
  end

  counter = 0
  (0..primes.count).each.with_index(2) do |item,index|
    unless primes[index]
      counter += 1
      return index if counter == n
    end

  end

end
