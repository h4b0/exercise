require "./4.rb"
    describe :nth_prime do
      it 'returns the nth prime number' do
        expect(nth_prime(3)).to eq(5)
        expect(nth_prime(6)).to eq(13)
        expect(nth_prime(10)).to eq(29)
        expect(nth_prime(13))
        expect(nth_prime(10001)).to eq(104743)
      end
    end
