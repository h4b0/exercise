#!/usr/bin/ruby
def exer1 n
  return (0..n).find_all{|i| i%3 == 0 || i%5 ==0}.reduce(:+)
end

