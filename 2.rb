#!/usr/bin/ruby
def prime?(n)
  ((2..(Math.sqrt(n)))).each do |i|
    return false if n % i == 0
  end

  return true
end
def find_number(a)
  count = 3
  index = 1
  while true
    index += 1 if prime?(count)
    return count if index == a
    count += 2
  end
end

require 'rspec'
    describe :find_number do
      it 'returns the nth prime number' do
        expect(find_prime(3)).to eq(5)
        expect(find_prime(6)).to eq(13)
        expect(find_prime(10)).to eq(29)
        expect(find_prime(10001)).to eq(104743)
      end
    end
